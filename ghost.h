#ifndef GHOST_H
#define GHOST_H

class ghost {
    public:
        ghost(int r, int g, int b, int row=0, int col=0);
        ~ghost();
        int getRow();
        int getCol();
        bool getPast();
        int getInitialRow();
        int getInitialCol();
        int getOutRow();
        int getOutCol();
        void setRow(int r);
        void setCol(int c);
        void setOutRow(int r);
        void setOutCol(int c);
        void setPast(bool p);
        void setColor(int r, int g, int b);
        void drawGhost();
        void setOut();
    private:
        int row;
        int col;
        int initialRow;
        int initialCol;
        int outRow;
        int outCol;
        int red;
        int green;
        int blue;
        bool past; // true for '.', false for ' '
};
#endif
