// Chau-Nhi Phan && Aemile Donoghue
// Section 05
// PACMAN game

#include"gfxnew.h"
#include<unistd.h>
#include<fstream>
#include<iostream>
#include<ctime>
#include<string>
using namespace std;
#include"board.h"
#include"ghost.h"

bool checkWin(Board &);
void resetG(Board &, int);
void releaseGhosts(int &, int &, int &, int &, Board &, int[]);

int main(){
  // initialize variables
  bool open = true, first = true;
  int r, g, b;
  int xc = 100;
  int yc = 300;
  int xcpacman = 364, ycpacman = 598;
  char c = ' ';
  int angle = 30;
  int wait = 80; //iterations until ghosts are released
  int g1 = 0, g2 = wait, g3 = 1.5*wait, g4 = 2*wait;
  int dir[4];
  srand(time(NULL));

  // begin graphics
  gfx_open(754, 806, "PACMAN");
  gfx_clear_color(0,0,0); // set background to black
  gfx_clear();
  Board board(ghost(255,0,0, 15,12), ghost(102,255,255, 15,13), ghost(255,153,51, 15,14), ghost(255,204,229, 15,15));

  // game play
  while(c != 'q'){
    releaseGhosts(g1, g2, g3, g4, board, dir);
    board.resetIm();
    board.print();
    // DRAW AND ANIMATE PACMAN
    if (!board.getImmunity() || board.getimCount() == 2 || board.getimCount() == 4) { 
      r = 255; g = 255; b = 0; // yellow (not immune or immunity wearing off)
    }
    else { 
      r = 122; g = 173; b = 255; // blue (immune)
    }
   
    if (open){ // open mouth
      gfx_color(0,0,0);
      gfx_fill_rectangle(board.getPastPacX(), board.getPastPacY(), 26, 26);
      usleep(10000);
      gfx_color(r,g,b);
      gfx_fill_arc(board.getPacX(),board.getPacY(), 25, 25, angle, 300);
      open = false;
    }
   
    else { // closed mouth
      gfx_color(0,0,0);
      gfx_fill_rectangle(board.getPastPacX(), board.getPastPacY(), 26, 26);
      usleep(10000);
      gfx_color(r,g,b);
      gfx_fill_arc(board.getPacX(), board.getPacY(), 25, 25, 0, 360);
      open = true;
    }

    gfx_flush();
    usleep(100000);
    if (gfx_event_waiting()){
      c = gfx_wait();
      for (int i = 0; i < 4; i++) {
        board.moveG(dir[i],board.getG(i+1));
      }
      switch(c){
        case 'R': // up arrow
          board.move(c);
          angle = 30+90;
          break;
        case 'Q': // left arrow
          board.move(c);
          angle = 30+180;
          break;
        case 'T': // down arrow
          board.move(c);
          angle = 30+270;
          break;
        case 'S': // right arrow
          board.move(c);
          angle = 30;
          break;
        default:
          board.setPastPacX();
          board.setPastPacY();
      }
 
   }
    else {
      board.setPastPacX();
      board.setPastPacY();
      board.move(c);
      for (int i = 0; i < 4; i++) { // iterates through all ghosts
        board.moveG(dir[i],board.getG(i+1));
        int num = rand()%4+1;
        if (num == 1) // randomly change direction 1/4 of the time
          dir[i] = rand()%4+1;
        if (dir[i] > 5) 
          dir[i] = 1;
      }
    }
    board.collision(board.getG(1));
    board.collision(board.getG(2));
    board.collision(board.getG(3));
    board.collision(board.getG(4));
  if(checkWin(board))
    c = 'q';
  }
  return 0;
}

bool checkWin(Board &board) { // determnines when and how game ends
  if (board.getDots() < 0){
    gfx_clear();
    char text[] = "CONGRATULATIONS!";
    gfx_changefont("12x24"); // make text bigger
    gfx_text(300, 330, text);
    gfx_flush();
    usleep(2000000);
    return true;
  }
  else if (board.getOver()){
    gfx_clear();
    char text[] = "GAME OVER";
    string score = "Score: " + to_string((272-board.getDots())*100);
    gfx_changefont("12x24"); // make text bigger
    gfx_text(320,330, text);
    gfx_text(310, 370, score.c_str());
    gfx_flush();
    usleep(2000000);
    return true;
  }
}

void resetG(Board &board, int g){ // draws black square over ghost when eaten and draws it back in the dome
  gfx_color(0,0,0);
  gfx_fill_rectangle((board.getG(g).getCol())*26,(board.getG(g).getRow())*26,26,26);
  char spot = board.getStatus(board.getG(g).getOutRow(), board.getG(g).getOutCol());
  if (spot == '.' || spot == 'I') 
    board.getG(g).setPast(true);
  else if (spot == ' ') 
    board.getG(g).setPast(false);
  board.getG(g).setOut();
  board.setStatus(board.getG(g).getRow(), board.getG(g).getCol(), 'X');
}
 
void releaseGhosts(int &g1, int &g2, int &g3, int &g4, Board &board, int dir[]){
    int wait = 80;
    
    if (g1 == 0) {
      resetG(board, 1);
      dir[0] = 1;
      g1 = wait;
    }
    else if(board.getG(1).getCol() == 16 && board.getG(1).getRow() == 15){ // dome position
      g1--;
    }
    
    if (g2 == 0) {
      resetG(board, 2);
      dir[1] = 3;
      g2 = wait;
    }
    else if(board.getG(2).getCol() == 13 && board.getG(2).getRow() == 15){
      g2--;
    }
    
    if (g3 == 0) {
      resetG(board, 3);
      dir[2] = 1;
      g3 = wait;
    }
    else if(board.getG(3).getCol() == 14 && board.getG(3).getRow() == 15){
      g3--;
    }
    
    if (g4 == 0) {
       resetG(board, 4);
       dir[3] = 3;
       g4 = wait;
    }
    else if(board.getG(4).getCol() == 15 && board.getG(4).getRow() == 15){
       g4--;
    }
}
