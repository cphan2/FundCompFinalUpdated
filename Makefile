
CMP = g++ -std=c++14 -lX11
CLASS = board
MAIN = final
EXEC = pacman

$(EXEC): $(CLASS).o $(MAIN).o gfxnew.o ghost.o
	$(CMP) $(CLASS).o $(MAIN).o gfxnew.o ghost.o -o $(EXEC)

$(CLASS).o: $(CLASS).cpp $(CLASS).h
	$(CMP) -c $(CLASS).cpp -o $(CLASS).o

$(MAIN).o: $(MAIN).cpp $(CLASS).h gfxnew.o
	$(CMP) -c $(MAIN).cpp gfxnew.o -o $(MAIN).o

ghost.o: ghost.cpp ghost.h
	$(CMP) -c ghost.cpp

clean:
	rm $(MAIN).o
	rm $(EXEC)

