#include "ghost.h"
#include "gfxnew.h"

ghost::ghost(int r, int g, int b, int row, int col){
    setColor(r, g, b);
    setRow(row);
    setCol(col);
    setPast(true);
    initialRow = row;
    initialCol = col;
}

ghost::~ghost(){}

int ghost::getRow(){
    return row;
}

int ghost::getCol(){
    return col;
}

bool ghost::getPast(){
    return past;
}

int ghost::getInitialRow(){
    return initialRow;
}

int ghost::getInitialCol(){
    return initialCol;
}

int ghost::getOutRow(){
    return outRow;
}

int ghost::getOutCol(){
    return outCol;
}

void ghost::setRow(int r){
    row = r;
}

void ghost::setCol(int c){
    col = c;
}

void ghost::setOutRow(int r){
    outRow = r;
}

void ghost::setOutCol(int c){
    outCol = c;
}

void ghost::setPast(bool p){
    past = p;
}

void ghost::setColor(int r, int g, int b){
    red = r;
    green = g;
    blue = b;
}

void ghost::drawGhost(){
    int xc = col*(26);
    int yc = row*(26);
    gfx_color(red, green, blue);
    gfx_fill_arc(xc+3, yc, 23, 23, 0, 180); // body
    gfx_fill_rectangle(xc+3, yc+10, 23, 9);
    short int x1 = xc + 3, x2 = xc + 9, x3 = xc + 19, x4 = xc + 26, x5 = xc + 13;
    short int y1 = yc + 17, y2 = yc + 25;
    XPoint left[] = { {x1, y1}, {x1, y2}, {x2, y1} };
    XPoint middle[] = { {x2, y1}, {x3, y1}, {x5, y2} };
    XPoint right[] = { {x3, y1}, {x4, y1}, {x4, y2} };
    int size = sizeof(left)/sizeof(XPoint);
    gfx_fill_polygon(left, size);
    gfx_fill_polygon(middle, size);
    gfx_fill_polygon(right, size);
    gfx_color(255,255,255); // eyes
    gfx_fill_circle(xc+9,yc+13,4);
    gfx_fill_circle(xc+20,yc+13,4);
    gfx_color(0,0,153);
    gfx_fill_circle(xc+9,yc+14,2);
    gfx_fill_circle(xc+21,yc+14,2);
}

void ghost::setOut(){
    row = outRow;
    col = outCol;
    drawGhost();
}
