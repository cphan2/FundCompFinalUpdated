// Chau-Nhi Phan & Aemile Donoghue
// Section 05 
// board.cpp - implementation for Board class
 
#include<iostream>
#include<cstdlib>
#include<unistd.h>
using namespace std;
#include "gfxnew.h"
#include"board.h"
#include "ghost.h"

Board::Board(ghost red, ghost blue, ghost orange, ghost pink) : g1(red), g2(blue), g3(orange), g4(pink){

  dots = 272; // total number of dots on board
  over = false;

  // initialize dots and walls
  for (int i = 0; i < 31; i++){
    for (int j = 0; j < 29; j++){
      board[i][j] = '.';
    }
  }
  for (int k = 0; k < 29; k++){
    board[0][k] = '_';
    board[30][k] = '_';
  }
  for (int k = 1; k < 31; k++){
    if (k != 14){
      board[k][0] = '|';
      board[k][28] = '|';
    }
  }
  
  // Block off Space for dome
  for (int i = 12; i < 17; i++){
    for (int j = 10; j < 19; j++){
      board[i][j] = '&';
    }
  }

  for(int j = 9; j < 20; j++){
    board[11][j] = ' ';
    board[17][j] = ' ';
  }
  for (int i = 11; i < 18; i++){
    board[i][9] = ' ';
    board[i][19] = ' ';
  }

  // Walls
  for (int i = 2; i < 5; i++){
    for (int n = 9; n < 13; n++){
      setStatus(i,14+n,'-');
      setStatus(i,14-n,'-');
      setStatus(6,14+n,'-');
      setStatus(6,14-n,'-');
      setStatus(7,14+n,'-');
      setStatus(7,14-n,'-');
      setStatus(21,14+n,'-');
      setStatus(21,14-n,'-');
      setStatus(22,14+n,'-');
      setStatus(22,14-n,'-');
    }
    for (int n = 3; n < 8; n++){
      setStatus(i,14+n,'-');
      setStatus(i,14-n,'-');
      setStatus(21,14+n,'-');
      setStatus(21,14-n,'-');
      setStatus(22,14+n,'-');
      setStatus(22,14-n,'-');
      for (int j = 9; j < 15; j++){
        setStatus(14+n-2,14+j,'-');
        setStatus(14-(n-2),14+j,'-');
        setStatus(14+n-2,14-j,'-');
        setStatus(14-(n-2),14-j,'-');
      }
    }
    for (int n = 13; n < 16; n++){
      setStatus(i,n,'-');
      setStatus(9,14+n-10,'-');
      setStatus(10,14+n-10,'-');
      setStatus(9,14-(n-10),'-');
      setStatus(10,14-(n-10),'-');
      for (int j = 1; j < 5; j++){
        setStatus(j,n,'-');
        setStatus(5+j,n,'-');
        setStatus(17+j,n,'-');
        setStatus(23+j,n,'-');
      } 
      setStatus(10,n,'-');
      setStatus(22,n,'-');
      setStatus(28,n,'-');
    }
  }
  for (int n = 10; n < 19; n++){
    setStatus(6,n,'-');
    setStatus(7,n,'-');
    setStatus(18,n,'-');
    setStatus(19,n,'-');
    setStatus(24,n,'-');
    setStatus(25,n,'-');
  }
  for (int i = 6; i < 8; i++){
    for (int j = 6; j < 14; j++){
      setStatus(j,14+i,'-');
      setStatus(j,14-i,'-');
    }
    for (int j = 15; j < 20; j++){
      setStatus(j,14+i,'-');
      setStatus(j,14-i,'-');
    }
    for (int j = 24; j < 29; j++){
      setStatus(j,14+i,'-');
      setStatus(j,14-i,'-');
    }
    for (int j = 3; j < 13; j++){
      setStatus(21+i,14+j,'-');
      setStatus(21+i,14-j,'-');
    }
    for (int j = 23; j < 26; j++){
      setStatus(j,14+i+3,'-');
      setStatus(j, 14-(i+3),'-');
    }
    for (int j = 24; j < 26; j++){
      setStatus(j,14+i+6,'-');
      setStatus(j,14-(i+6),'-');
    }
  }

  // Immunity Dots
  setStatus(3,1,'I');  
  setStatus(3,27,'I');  
  setStatus(23,1,'I');  
  setStatus(23,27,'I'); 
  imCount = 55; 

  // GRAPHICS
  drawMaze();
  drawDome();

  // Pacman's starting position
  pacrow = 23; pastpacr = 23;
  paccol = 14; pastpacc = 14;
  setPac();

  // Ghosts
  g1.setOutRow(10); g1.setOutCol(12);
  g2.setOutRow(18); g2.setOutCol(9);
  g3.setOutRow(10); g3.setOutCol(16);
  g4.setOutRow(18); g4.setOutCol(19);
  g1.drawGhost();
  g2.drawGhost();
  g3.drawGhost();
  g4.drawGhost();
  setStatus(g1.getRow(),g1.getCol(),'X'); 
  setStatus(g2.getRow(),g2.getCol(),'X'); 
  setStatus(g3.getRow(),g3.getCol(),'X'); 
  setStatus(g4.getRow(),g4.getCol(),'X'); 

}
 
Board::~Board() {}

//moves pacman to that position 
bool Board::move(char arrow) {
  int row2 = pacrow, col2 = paccol, row1 = pacrow, col1=paccol;
  bool temp = true;
  switch(arrow){
    // adjust point based on arrow pressed
    case 'R': // up arrow
      row2--;
      break;
    case 'Q': // left arrow
      col2--;
      if (col2 < 0)
        col2 = 28;
      break;
    case 'T': // down arrow
      row2++;
      break;
    case 'S': // right arrow
      col2++;
      if (col2 > 28)
        col2 = 0;
      break;
  }
  switch(getStatus(row2, col2)) {
    case ' ': // move pacman there
      pastpacr = pacrow; pastpacc = paccol;
      pacrow = row2; paccol = col2;
      setPac(); // set pacman to be at that position
      setStatus(row1, col1, ' ');
      return true;
      break;
    case 'I': // do everything as normal but set immunity to true first (NO BREAK)
      setImmunity(true);
      imCount += 50;
    case '.': // move pacman there and remove the dot there
      pastpacr = pacrow; pastpacc = paccol;
      pacrow = row2; paccol = col2;
      setPac();
      setStatus(row1, col1, ' ');
      dots--;
      return true;
    case '|': // pacman can't move there so don't do anything
    case '_': // pacman can't move there so don't do anything
    case '-': // pacman can't move there so don't do anything
      temp = false;
      return false;
      break;
    case 'X': // ghost
      pastpacr = pacrow; pastpacc = paccol;
      pacrow = row2; paccol = col2;
      setPac();
      setStatus(row1, col1, ' ');
      return true;
      break;
  }
}

bool Board::moveG(int n, ghost g) {
  int nrow = g.getRow(), ncol = g.getCol();
  switch(n){
    case 1: // up arrow
      nrow--;
      break;
    case 2: // left arrow
      ncol--;
      if (ncol < 0) ncol = 28;
      break;
    case 3: // down arrow
      nrow++;
      break;
    case 4: // right arrow
      ncol++;
      if (ncol > 28) ncol = 0;
      break;
  }

  switch(getStatus(nrow, ncol)){
    case '|': 
    case '_': 
    case '-': 
      return false;
      break;
    
    // draw black square first
    gfx_color(0,0,0);
    gfx_fill_rectangle(g.getCol()*26,g.getRow()*26,26,26);
    char replace;

    case ' ': // move ghost
      if (g.getPast()){
        gfx_color(252,179,63);
        gfx_fill_rectangle(26*g.getCol()+10,26*g.getRow()+10, 6, 6);
        replace = '.';
      }
      else {
        replace = ' ';
      }
      setStatus(g.getRow(),g.getCol(),replace);
      g.setRow(nrow); g.setCol(ncol);
      g.drawGhost();
      board[g.getRow()][g.getCol()] = 'X';
      g.setPast(false);
      return true;
      break;
    case 'I':
      if (g.getPast()){
        gfx_color(255,255,255);
        gfx_fill_circle(g.getCol()*26+13,g.getRow()*26+13,8);
        replace = '.';
      }
      else {
        replace = ' ';
      }
      setStatus(g.getRow(),g.getCol(),replace);
      g.setRow(nrow); g.setCol(ncol);
      g.drawGhost();
      board[g.getRow()][g.getCol()] = 'X';
      g.setPast(true);
      return true;
      break;
    case '.': // move ghost
      if (g.getPast()){
        gfx_color(252,179,63);
        gfx_fill_rectangle(26*g.getCol()+10,26*g.getRow()+10, 6, 6);
        replace = '.';
      }
      else {
        replace = ' ';
      }
      setStatus(g.getRow(),g.getCol(),replace);
      g.setRow(nrow); g.setCol(ncol);
      g.drawGhost();
      board[g.getRow()][g.getCol()] = 'X';
      g.setPast(true);
      return true;
      break;
    case 'O': // pacman
      g.setRow(nrow); g.setCol(ncol);
      g.drawGhost();
      board[g.getRow()][g.getCol()] = 'X';
      break;
  }      
}
      
char Board::getStatus(int row, int col) {
  return board[row][col];
}

// sets the given position to something based on the third int passed to it (right now only sets it to pacman)
void Board::setStatus(int row, int col, char c){
  if (c == 'O') { // move pacman to that position
    pacrow = row; paccol = col; 
    setPac();
  }
  else {
    board[row][col] = c;
  }
}

// used only for debugging
void Board::print() {
  for (int i = 0; i < 31; i++){
    for (int j = 0; j < 29; j++){
      cout << board[i][j];
    }
    cout << endl;
  }
}

// sets board to 'O' at pacman's position
void Board::setPac() {
  board[pacrow][paccol] = 'O';
}

int Board::getPacX() {
  return paccol*26;
}

int Board::getPacY() {
  return pacrow*26;
}

int Board::getPastPacX() {
  return pastpacc*26;
}

int Board::getPastPacY() {
  return pastpacr*26;
}

void Board::setPastPacX() {
  pastpacc = paccol;
}

void Board::setPastPacY() {
  pastpacr = pacrow;
}

int Board::getDots() {
  return dots;
}

// draw the board with graphics - only called at start of game
void Board::drawMaze() {
  bool statusline;
  gfx_color(0,0,255);
  for (int p = 0; p < 31; p++){
    for (int q = 0; q < 29; q++){
      if ((getStatus(p,q) == '|') || (getStatus(p,q) == '-') || (getStatus(p,q) == '_')){
        gfx_color(0,0,255);
        gfx_fill_rectangle(26*q,26*p, 26, 26);
      }
      if (getStatus(p,q) == '.'){
        gfx_color(252,179,63);
        gfx_fill_rectangle(26*q+10,26*p+10, 6, 6);
      }
      if (getStatus(p,q) == 'I'){
        gfx_color(255,255,255);
        gfx_fill_circle(26*q+13,26*p+13,8);
      }
    }
  }
}

// draw dome once at start of game
void Board::drawDome() {
  int xcdome = 260;
  int ycdome = 312+13;
  gfx_color(204, 204, 0);
  gfx_fill_arc(xcdome+91, ycdome+5, 50, 70, 0, 180);
  gfx_fill_arc(xcdome+19+91, ycdome-27+15, 12, 20, 0, 360);
  gfx_color(153,100,0);
  gfx_fill_rectangle(xcdome+1+91, ycdome+39, 49, 30);
  gfx_fill_rectangle(299-13, 377, 156+26, 65);
  gfx_color(0,0,0);
  gfx_fill_rectangle(312, 390, 134, 52);
  gfx_color(102,51,0);
  gfx_fill_rectangle(xcdome+1+91, ycdome+39, 49, 5);
  gfx_line(xcdome+16+91, ycdome+40, xcdome+16+91, 389);
  gfx_line(xcdome+35+91, ycdome+40, xcdome+35+91, 389);
  gfx_line(xcdome+91, ycdome+40, xcdome+91, 389);
  gfx_line(xcdome+49+91, ycdome+40, xcdome+49+91, 389);
  XPoint x[] = { {377, 370} , {351, 390} , {400, 390}, {377, 370}};
  int size = sizeof(x)/sizeof(XPoint);
  gfx_fill_polygon(x, size);
}

// determine if a collision occurred
void Board::collision(ghost g){
  if (g.getCol() == paccol && g.getRow() == pacrow) {
    eat(g);
  }
}

void Board::eat(ghost g){
  if (getImmunity()){ 
    // if pacman is immune, the ghost should get eaten and sent back to the dome
      sendDome(g);
      dots--;
  }
  else { // pacman is not in immuity mode and gets eaten
    over = true;
  }
}

void Board::sendDome(ghost g){
  g.setRow(g.getInitialRow());
  g.setCol(g.getInitialCol());
  board[g.getRow()][g.getCol()] = 'X';
  g.drawGhost();

}

bool Board::getImmunity(){
  return immunity;
}

void Board::setImmunity(bool im){
  immunity = im;
}

ghost Board::getG(int n){
    switch(n){
      case 1:
        return g1;
      case 2:
        return g2;
      case 3:
        return g3;
      case 4:
        return g4;
    }
}

void Board::resetIm(){
  if (imCount > 0 && immunity) imCount--;
  else if (imCount == 0) {
    immunity = false;
    imCount = 50;
  }
}

int Board::getimCount(){
  return imCount;
}

bool Board::getOver(){
  return over;
}
