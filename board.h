// Chau-Nhi Phan & Aemile Donoghue
// Section 05
// board.h Interface for the Board Class for the pacman game

#ifndef BOARD_H
#define BOARD_H

#include "ghost.h"

class Board {
  public:
    Board(ghost, ghost, ghost, ghost);
    ~Board();
	
    bool move(char); // moves pacman to that position if get status returns true
    bool moveG(int, ghost); // direction, ghost
    char getStatus(int, int); // returns an int corresponding to what is located at a position
    void setStatus(int, int, char); // sets something to be located on a position. That thing is determined by the 3rd int passed
    void print();
    void setPac();
    void setG(ghost);
    int getPacX();
    int getPacY();
    int getPastPacX();
    int getPastPacY();
    void setPastPacX();
    void setPastPacY();
    int getDots();
    ghost getG(int);
    void drawMaze();
    void drawDome();
    void collision(ghost); // moving character
    void eat(ghost);
    bool getImmunity();
    void setImmunity(bool);
    void sendDome(ghost);
    void resetIm();
    int getimCount();
    bool getOver();
  private:
    char board[31][29];
    int pacrow, paccol; // pacman's current position
    int pastpacr, pastpacc; // pacman's previous position
    int grow[4], gcol[4]; // ghost coordinates
    bool past[4]; // true for '.', false for ' '
    int dots;
    bool immunity = false;
    int imCount;
    bool over;
    ghost g1; // red ghost
    ghost g2; // blue ghost
    ghost g3; // orange ghost
    ghost g4; // pink ghost
};
#endif
